package org.gcube.vremanagement.contextmanager.handlers;

import java.util.List;

import org.gcube.vremanagement.contextmanager.exceptions.ContextAlreadyExistsException;
import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidContextException;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.gcube.vremanagement.contextmanager.model.types.ContextRetriever;


public interface ContextContainer extends ContextRetriever{
	
	List<String> getAvailableContexts();
	
	void addContext(Context toAdd) throws InvalidContextException, ContextAlreadyExistsException;
	
	void removeContext(String contextId) throws InvalidContextException;
	
	boolean addResource(String contextId, String resourceId) throws InvalidContextException;
	
	boolean removeResource(String contextId, String resourceId) throws InvalidContextException;
	
	List<String> getResources(String contextId) throws InvalidContextException;
}
