package org.gcube.vremanagement.contextmanager.handlers.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.gcube.vremanagement.contextmanager.model.types.Context;

public class TreeItem {

	TreeItem parent;
	Context context;
	
	private Set<TreeItem> children = new HashSet<>();
	
	public TreeItem(TreeItem parent, Context context) {
		this.parent = parent;	
		this.context = context;
	}

	public void addChild(TreeItem child) {
		children.add(child);
	}
	
	public void removeChild(TreeItem child) {
		children.remove(child);
	}
	
	public Set<TreeItem> getChildren() {
		return Collections.unmodifiableSet(children);
	}

	public TreeItem getParent() {
		return parent;
	}
	
	public Context getContext() {
		return context;
	}
	
	public boolean isLeaf() {
		return children.size()==0;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((context == null) ? 0 : context.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TreeItem other = (TreeItem) obj;
		if (context == null) {
			if (other.context != null)
				return false;
		} else if (!context.equals(other.context))
			return false;
		return true;
	}
	
}
