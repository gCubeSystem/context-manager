package org.gcube.vremanagement.contextmanager.handlers.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.gcube.vremanagement.contextmanager.exceptions.ContextAlreadyExistsException;
import org.gcube.vremanagement.contextmanager.handlers.ContextContainer;
import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidContextException;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.gcube.vremanagement.contextmanager.model.types.ContextRetriever;

@Singleton
@Default
public class ContextContainerImpl implements ContextContainer, ContextRetriever {

	
	private Map<String, List<String>> resourceMap = new HashMap<>();

	@Inject ContextTree contextTree;
	
	/*private static ContextTree contextTree = new ContextTree() {
		
		@Override
		public void init() {
			
		}
	};*/
	
	public List<String> getAvailableContexts() {
		return contextTree.getContexts();
	}

	public void addContext(Context toAdd) throws InvalidContextException, ContextAlreadyExistsException{
		contextTree.createItem(toAdd.getParent()==null?null:toAdd.getParent().getId(), toAdd);
	}

	public void removeContext(String contextId) throws InvalidContextException {
		TreeItem item = contextTree.removeItem(contextId);
		if (item==null) throw new InvalidContextException("context with id "+contextId+" not found");
		resourceMap.remove(item.getContext().getId());
	}

	public Context getContextById(String id) throws InvalidContextException {
		Context context = contextTree.getContext(id);
		if (context==null) throw new InvalidContextException("context with id "+id+" not found");
		return context;
	}

	public synchronized boolean addResource(String contextId, String resourceId) throws InvalidContextException {
		Context context = contextTree.getContext(contextId);
		if (context==null) throw new InvalidContextException("context with id "+contextId+" not found");

		if (!resourceMap.containsKey(contextId))
			resourceMap.put(contextId, new ArrayList<>()); 
		return resourceMap.get(contextId).add(resourceId);
		
	}

	public synchronized boolean removeResource(String contextId, String resourceId) throws InvalidContextException {
		Context context = contextTree.getContext(contextId);
		if (context==null) throw new InvalidContextException("context with id "+contextId+" not found");

		if (resourceMap.containsKey(contextId))
			return resourceMap.get(contextId).remove(resourceId);
		else return false;
	}

	public List<String> getResources(String contextId) throws InvalidContextException {
		Context context = contextTree.getContext(contextId);
		if (context==null) throw new InvalidContextException("context with id "+contextId+" not found");
		if (resourceMap.containsKey(contextId))
			return resourceMap.get(contextId);
		else return Collections.emptyList();
	}

}


	