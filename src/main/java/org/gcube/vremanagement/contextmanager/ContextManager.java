package org.gcube.vremanagement.contextmanager;

import java.util.List;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.gcube.common.authorization.library.provider.AuthorizationProvider;
import org.gcube.vremanagement.contextmanager.exceptions.ContextAlreadyExistsException;
import org.gcube.vremanagement.contextmanager.handlers.ContextContainer;
import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidContextException;
import org.gcube.vremanagement.contextmanager.model.operators.context.CustomContextOperator;
import org.gcube.vremanagement.contextmanager.model.operators.context.MandatoryContextOperator;
import org.gcube.vremanagement.contextmanager.model.operators.parameters.OperatorParameters;
import org.gcube.vremanagement.contextmanager.model.report.OperationResult;
import org.gcube.vremanagement.contextmanager.model.report.Report;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.gcube.vremanagement.contextmanager.model.types.Context.Type;
import org.slf4j.Logger;


@Singleton
public class ContextManager {
	
	@Inject Logger log;
	
	@Inject
	@Any
	private Instance<MandatoryContextOperator> mandatoryContextOperators;
	
	@Inject
	@Any
	private Instance<CustomContextOperator<?,?>> customContextOperators;
	
	@Inject
	private ContextContainer contextContainer;
	
	
	public Report createContext(String parentContextId, String vreName, List<OperatorParameters> parameters, String user) throws InvalidContextException {
		
		Context parentContext = contextContainer.getContextById(parentContextId);
		
		log.debug("parent context is null?? {}",(parentContext==null));
		
		Context newContext = new Context(parentContext, vreName, vreName, Type.VRE);
		
		Report report = new Report(newContext.getId(), vreName, newContext.getType(), "createContext", user );
		
		try {
			contextContainer.addContext(newContext );
		} catch (ContextAlreadyExistsException e) {
			String errorMsg = String.format("the context with id %s already exists", vreName);
			report.setResult(OperationResult.failure(errorMsg));
			return report;
		}
		
		
		// TODO: run all the operation passed
		
		report.setResult(OperationResult.success());
		return report;
	
	}
	
	public Report disposeContext(String contextId) throws InvalidContextException {
		Context context = contextContainer.getContextById(contextId);
		contextContainer.removeContext(contextId);
		mandatoryContextOperators.forEach(co -> co.onDispose(context));
		return new Report(contextId, context.getName(), context.getType(), "disposeContext", AuthorizationProvider.instance.get().getClient().getId());
	}
	
	public List<String> getAvailableContexts(){
		return contextContainer.getAvailableContexts();
	}
	
	public List<String> getAssociatedResources(String contextId)  throws InvalidContextException{
		return contextContainer.getResources(contextId);
	}
}
