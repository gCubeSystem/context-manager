package org.gcube.vremanagement.contextmanager;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.Path;
import javax.ws.rs.core.Application;

import org.gcube.vremanagement.contextmanager.services.ContextService;
import org.glassfish.jersey.media.multipart.MultiPartFeature;

@Path("")
public class Service extends Application {

	@Override
    public Set<Class<?>> getClasses() {
        final Set<Class<?>> classes = new HashSet<Class<?>>();
        // register resources and features
        classes.add(ContextService.class);
        classes.add(MultiPartFeature.class);
        return classes;
    }

	
}
