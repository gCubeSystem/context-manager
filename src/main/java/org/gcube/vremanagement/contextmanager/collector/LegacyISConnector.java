package org.gcube.vremanagement.contextmanager.collector;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status.Family;

import org.gcube.common.resources.gcore.GCoreEndpoint;
import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.resources.gcore.HostingNode;
import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resource.Type;
import org.gcube.common.resources.gcore.Resources;
import org.gcube.common.resources.gcore.ServiceEndpoint;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.common.scope.api.ServiceMap;
import org.gcube.common.scope.impl.ScopedServiceMap;
import org.gcube.vremanagement.contextmanager.Utils;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.slf4j.Logger;

@Singleton
@Default
public class LegacyISConnector  {

	@Inject Logger log;

	private Client client = ClientBuilder.newClient();

	private static final String RESOURCE_SERVICE = "XMLStoreService";
	
	public Resource find(String resourceId) {
		WebTarget webTarget = getWebTarget().path(resourceId);
		String res = webTarget.request(MediaType.APPLICATION_XML).get(String.class);
		return parseResource(res);
	}

	public void createContext(Context context, String parentContextId, List<String> resourceIds) {
	}

	public boolean removeContext(Context context) {
		return true;
	}

	public boolean addResourceToContext(Context context, Resource resource) {
		String completeContext = Utils.getScopeFromContext(context);
		resource.scopes().asCollection().add(completeContext);
		
		WebTarget webTarget = getWebTarget().path(resource.type().toString()).path(resource.id());
		StringWriter writer = new StringWriter();
		Resources.marshal(resource, writer);
		Response response = webTarget.request(MediaType.TEXT_XML).put(Entity.entity(writer.toString(), MediaType.TEXT_XML));
		return response.getStatusInfo().getFamily()==Family.SUCCESSFUL;
	}

	public boolean removeResourceFromContext(Context context, Resource resource) {
		
		String completeContext = Utils.getScopeFromContext(context);
		resource.scopes().asCollection().remove(completeContext);

		WebTarget webTarget = getWebTarget().path(resource.type().toString()).path(resource.id());

		Response response;
		if (resource.scopes().size()>0) {
			StringWriter writer = new StringWriter();
			Resources.marshal(resource, writer);
			response = webTarget.request(MediaType.TEXT_XML).put(Entity.entity(writer.toString(), MediaType.TEXT_XML));
			return response.getStatusInfo().getFamily()==Family.SUCCESSFUL;
		} else 
			response = webTarget.request().delete();
		
		return response.getStatusInfo().getFamily()==Family.SUCCESSFUL;
	}

	public boolean updateResource(Resource resource) {
		// TODO Auto-generated method stub
		return false;
	}

	
	private WebTarget getWebTarget() {
		ServiceMap map = ScopedServiceMap.instance;
		String service = map.endpoint(RESOURCE_SERVICE);
		
		return client.target(service).queryParam("gcube-scope", ScopeProvider.instance.get());
	}
	
	private Resource parseResource(String res) {
		String typeString = res.substring(res.indexOf("<Type>")+"<Type>".length(), res.indexOf("</Type>"));
		Type type = null;
		for (Type resType : Type.values()) 
			if (resType.toString().equals(typeString)) {
				type = resType;
				break;
			}
		
		
		Class<? extends Resource> resClass = null;
		switch (type) {
		case ENDPOINT:
			resClass = ServiceEndpoint.class;
			break;
		case GCOREENDPOINT:
			resClass = GCoreEndpoint.class;
			break;
		case NODE:
			resClass = HostingNode.class;
			break;
		case GENERIC:
			resClass = GenericResource.class;
			break;
		default:
			break;	
		}
		
		return  Resources.unmarshal(resClass, new StringReader(res));		
	}
	
}
