package org.gcube.vremanagement.contextmanager;

import java.io.StringReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

public class ResourceBinder {

	private JAXBContext jaxbContext;
	
	private static ResourceBinder binder = null;
	
	
	public static synchronized ResourceBinder get() {
		if (binder==null) {
			try {
				JAXBContext context = JAXBContext.newInstance(ScopeDescriptor.class);
				binder= new ResourceBinder(context);
			} catch (JAXBException e) {
				throw new RuntimeException("cannot create jaxbContext",e);
			}
		} 
		return binder;
	}
	
	private ResourceBinder(JAXBContext context) {
		this.jaxbContext = context;
	}
	
	public ScopeDescriptor bind(String xml) throws JAXBException{
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		ScopeDescriptor scopedRes = (ScopeDescriptor)unmarshaller.unmarshal(new StringReader(xml));
		return scopedRes;
	}

}
