package org.gcube.vremanagement.contextmanager;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.LinkedList;
import java.util.List;

import javax.enterprise.inject.Default;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.xml.bind.JAXBException;

import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.scope.impl.ScopeBean;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.gcube.vremanagement.contextmanager.exceptions.ContextAlreadyExistsException;
import org.gcube.vremanagement.contextmanager.handlers.ContextContainer;
import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidContextException;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.gcube.vremanagement.contextmanager.model.types.Context.Type;
import org.gcube.vremanagement.contextmanager.model.types.ScopedResource;
import org.slf4j.Logger;

@Singleton
public class ScopeInitializer {

	@Inject Logger logger;
	
	@Inject
	@Default
	ContextContainer contextContainer;	
	
	public void initScope(String currentContext) {
		logger.debug("current context is {}",currentContext);
		ScopeBean currentScopeBean = new ScopeBean(currentContext);
		if (currentScopeBean.is(ScopeBean.Type.VRE)) return;
		try {
			createEntireContext(currentScopeBean);
		} catch (InvalidContextException e) {
			logger.error("invalid context {}", currentContext,e);
			return;
		} 


		logger.info("resource initialization started in {} ",currentContext);

		SimpleQuery query = queryFor(GenericResource.class);
		query.addCondition("$resource/Profile/SecondaryType/text() eq 'VRE' or $resource/Profile/SecondaryType/text() eq 'VO'");

		DiscoveryClient<GenericResource> client = clientFor(GenericResource.class);
		List<GenericResource> resources = client.submit(query);
		for (GenericResource resource : resources) {
			try {
				String body = resource.profile().bodyAsString();
				ScopeDescriptor scopeDescr = ResourceBinder.get().bind("<Body>"+body+"</Body>");
				String scope  = scopeDescr.context;

				try {
					Context context = createEntireContext(new ScopeBean(scope));
					for (ScopedResource res : scopeDescr.scopedResources)
						contextContainer.addResource(context.getId(), res.getId());
				}  catch (InvalidContextException e) {
					logger.error("invalid context {}", scope,e);
				}
			} catch (JAXBException e) {
				logger.error("cannot unmarshal resource",e);
			}
		}
	}
	
	private Context createEntireContext(ScopeBean originalContextBean) throws InvalidContextException{
		LinkedList<String> contextList = new LinkedList<>();
		contextList.addFirst(originalContextBean.toString());
		ScopeBean enclosingScope = originalContextBean.enclosingScope();
		while (enclosingScope!=null) {
			logger.debug("adding scope {} to list",enclosingScope.name());
			String scope = enclosingScope.toString();
			contextList.addFirst(scope);
			enclosingScope = enclosingScope.enclosingScope();
		}
		
		logger.debug("context list is  {}",contextList);
		
		Context actual = null;
		for (String context : contextList) {
			logger.debug("adding context {}",context);
			int level = context.split("/").length-2;
			String name = context.substring(context.lastIndexOf("/")+1);
			logger.debug("adding context name {} and level is {}",name,level);
			try {
				Context parentContext = actual;
				actual = new Context(parentContext,context, name, Type.values()[level]);
				contextContainer.addContext(actual);
			} catch (ContextAlreadyExistsException e) {
				logger.warn("context {} already exists", context);
			} catch (InvalidContextException ice) {
				logger.warn(ice.getMessage());
			}
		}
			
		return actual;
	}
}
