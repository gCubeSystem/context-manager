package org.gcube.vremanagement.contextmanager;

import org.gcube.vremanagement.contextmanager.model.types.Context;

public class Utils {


	public static String getScopeFromContext(Context context) {
		Context contextInUse = context;
		String scope = String.format("/%s",contextInUse.getName());
		while (contextInUse.getParent()!= null) {
			contextInUse = contextInUse.getParent();
			scope = String.format("/%s%s",contextInUse.getName(), scope);
		}
		return scope;
	}
	
}
