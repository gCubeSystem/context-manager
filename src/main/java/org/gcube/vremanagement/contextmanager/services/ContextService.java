package org.gcube.vremanagement.contextmanager.services;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;

import org.gcube.common.authorization.library.provider.AuthorizationProvider;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.smartgears.annotations.ManagedBy;
import org.gcube.vremanagement.contextmanager.ContextManager;
import org.gcube.vremanagement.contextmanager.ContextServiceAppManager;
import org.gcube.vremanagement.contextmanager.Utils;
import org.gcube.vremanagement.contextmanager.handlers.ContextContainer;
import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidContextException;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.gcube.vremanagement.contextmanager.model.types.StringList;
import org.gcube.vremanagement.contextmanager.operators.resources.ResourceManager;
import org.glassfish.jersey.media.multipart.FormDataParam;
import org.slf4j.Logger;

@Path("contexts")
@ManagedBy(ContextServiceAppManager.class)
public class ContextService {

	@Inject Logger log;
	
	@RequestScoped
	@Inject
	ContextServiceAppManager appManager ;
	
	@Inject
	ContextManager contextHandler;

	@Inject
	ResourceManager resourceHandler;

	@Inject
	ContextContainer contextContainer;
	
	@RequestScoped
	@PathParam("contextId") 
	String contextId;
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("")
	public StringList getContexts() {
		log.info("get Context called");
		try {
			List<String> contexts = contextHandler.getAvailableContexts();
			log.debug("found {}",contexts);
			return new StringList(contexts);
		}catch (Exception e) {
			log.error("error retrieving contexts",e);
			throw new WebApplicationException(e);
		}
	}
	
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	@Path("/{contextId}")
	public void createVREContext(@FormDataParam("resources") StringList resourceIds, @FormDataParam("vreName") String vreName ){
		log.info("create VRE called");
		
		try {
			//validate Call
			contextHandler.createContext(contextId, vreName, resourceIds.getValues(), AuthorizationProvider.instance.get().getClient().getId());
		}catch (Exception e) {
			log.error("error creating context {}",contextId,e);
			throw new WebApplicationException(e);
		}
	}

	@DELETE
	@Path("{contextId}")
	public void disposeVREContext() {
		log.info("dispose VRE called");
		
		try {
			//Context context = contextContainer.getContextById(contextId);
			//validateCall(fullContext);
			
			contextHandler.disposeContext(contextId);
		}catch (Exception e) {
			log.error("error disposing context {}",contextId,e);
			throw new WebApplicationException(e);
		}
	}

	@DELETE
	@Path("/{contextId:(.*(?=/resources))}/resources/{resourceId}")
	public void removeResourceFromContext(@PathParam("resourceId") String resourceId) {
		log.info("remove resource {} from context {}", resourceId, contextId);
		try {
			Context context = contextContainer.getContextById(contextId);
			validateCall(Utils.getScopeFromContext(context));
			resourceHandler.removeResourceFromContext(context, resourceId);
		}catch (Exception e) {
			// TODO: handle exception
		} 
	}

	

	@PUT
	@Path("/{contextId:(.*(?=/resources))}/resources")
	public String addResourceToContext(String resourceId) {
		log.info("adding resource {} to context {}", resourceId, contextId);
		try {
			Context context = contextContainer.getContextById(contextId);
			validateCall(Utils.getScopeFromContext(context));
			if (appManager.isManaged(context))
				resourceHandler.addResourceToContext(context, resourceId);
			else {
				log.debug("the context {} is not managed", contextId);
				//TODO: else manage queue to add resource in another ContextManager
			}
		}catch (Exception e) {
			// TODO: handle exception
		} 
		return null;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{contextId:(.*(?=/resources))}/resources")
	public StringList getResources() {
		log.info("getResource in context {} called",contextId);
		
		try {
			//Context context = contextContainer.getContextById(contextId);
			//validateCall(fullContext);
			
			return new StringList(contextHandler.getAssociatedResources(contextId));
		}catch (Exception e) {
			log.error("error getting resource for context {}",contextId,e);
			throw new WebApplicationException(e);
		}
	}

	private void validateCall(String fullContext) throws InvalidContextException {
		String currentContext = ScopeProvider.instance.get();
		if (!fullContext.startsWith(currentContext))
			throw new InvalidContextException("invalid context");
	}
	
	

}
