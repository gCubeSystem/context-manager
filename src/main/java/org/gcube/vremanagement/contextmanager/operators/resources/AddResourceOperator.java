package org.gcube.vremanagement.contextmanager.operators.resources;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidContextException;
import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidParameterException;
import org.gcube.vremanagement.contextmanager.model.exceptions.OperationException;
import org.gcube.vremanagement.contextmanager.model.operators.context.CustomContextOperator;
import org.gcube.vremanagement.contextmanager.model.operators.parameters.AddResourceParameters;
import org.gcube.vremanagement.contextmanager.model.operators.parameters.OperatorParameters;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.gcube.vremanagement.contextmanager.model.types.Context.Type;
import org.gcube.vremanagement.contextmanager.model.types.ContextRetriever;
import org.gcube.vremanagement.contextmanager.model.types.ScopedResource;
import org.slf4j.Logger;

public class AddResourceOperator extends CustomContextOperator<ScopedResource, AddResourceParameters> {

	@Inject
	private Logger log;

	@Inject
	private ResourceManager resourceManager;

	@Override
	public Set<Type> getAllowedContextType() {
		return new HashSet<>(Arrays.asList(Type.values()));
	}

	@Override
	public String getOperationId() {
		return "add-resource";
	}

	@Override
	public String getDescription() {
		return "Adds resource to context";
	}

	@Override
	public ScopedResource execute(Context context, AddResourceParameters params) throws OperationException {
		log.debug("executing operation {}",this.getOperationId());
		try {
			ScopedResource res = resourceManager.addResourceToContext(context, params.getResourceId());
			return res;
		}catch (InvalidContextException e) {
			//it should never happen
			throw new InvalidParameterException("unknown exception");
		} 
	}

	@Override
	public ScopedResource undo(Context context, OperatorParameters params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected AddResourceParameters checkAndTrasformParameters(Context context, OperatorParameters params)
			throws InvalidParameterException {
		AddResourceParameters addParams = AddResourceParameters.class.cast(params);
		if (addParams.getResourceId()==null)
			throw new InvalidParameterException("resourceId is null");
		return addParams;
	}


}
