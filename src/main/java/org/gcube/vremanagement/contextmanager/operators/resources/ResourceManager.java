package org.gcube.vremanagement.contextmanager.operators.resources;

import java.util.Iterator;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resource.Type;
import org.gcube.vremanagement.contextmanager.collector.LegacyISConnector;
import org.gcube.vremanagement.contextmanager.handlers.ContextContainer;
import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidContextException;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.gcube.vremanagement.contextmanager.model.types.ScopedResource;
import org.jboss.weld.exceptions.IllegalArgumentException;
import org.slf4j.Logger;


@Singleton
public class ResourceManager {

	@Inject
	@Any
	Instance<ResourceHandler> resourcesHandlers;
	
	@Inject
	ContextContainer contextContainer;
	
	@Inject
	LegacyISConnector connector;
	
	@Inject Logger log;
	
	public ScopedResource addResourceToContext(Context context, String id) throws InvalidContextException {
		Resource res = retrieveResource(id);
		ResourceHandler handler = retrieveHandler(res.type());
		handler.addResource(context, res);
		contextContainer.addResource(context.getId(), id);
		return getScopedResource(res);
		
	}

	public ScopedResource removeResourceFromContext(Context context, String id ) throws InvalidContextException {
		Resource res = retrieveResource(id);
		ResourceHandler handler = retrieveHandler(res.type());
		handler.removeResource(context, res);
		contextContainer.removeResource(context.getId(), id);
		return getScopedResource(res);
	}

	private Resource retrieveResource(String id) {
		return connector.find(id);
		
	}

	private ResourceHandler retrieveHandler(Type type) {
		Iterator<ResourceHandler> it = resourcesHandlers.iterator();
		while (it.hasNext()) {
			ResourceHandler rh = it.next();
			log.debug("handler {} found",rh.getClass().getSimpleName());
			if (rh.getManagedResources().contains(type))
				return rh;
		}

		throw new IllegalArgumentException("handler not found");
	}

	private ScopedResource getScopedResource(Resource res) {
		return new ScopedResource(res.id(), res.type().name(), Long.toString(System.currentTimeMillis()));
	}

	
}
