package org.gcube.vremanagement.contextmanager.operators.resources;


import java.util.List;

import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resource.Type;
import org.gcube.vremanagement.contextmanager.model.types.Context;

public interface ResourceHandler {

	List<Type> getManagedResources();
	
    boolean addResource(Context context, Resource resource);
	
	boolean removeResource(Context context, Resource resource);
	
	//in case of resource created directly by the publisher
	void createResourceNotified(Context context, Resource resource);
	
	//in case of resource deleted directly by the publisher
	void removeResourceNotified(Context context, Resource resource);
	
}
