package org.gcube.vremanagement.contextmanager.operators.resources;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resource.Type;
import org.gcube.vremanagement.contextmanager.collector.LegacyISConnector;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.slf4j.Logger;

@Singleton
public class StaticResourceHandler implements ResourceHandler {

	@Inject
	Logger log;
	
	@Override
	public List<Type> getManagedResources() {
		return Arrays.asList(Type.GENERIC, Type.ENDPOINT);
	}

	@Inject
	LegacyISConnector connector;
	
	@Override
	public boolean addResource(Context context, Resource resource) {
		return connector.addResourceToContext(context, resource);
	}

	@Override
	public boolean removeResource(Context context, Resource resource) {
		return connector.removeResourceFromContext(context, resource);
	}

	@Override
	public void createResourceNotified(Context context, Resource resource) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeResourceNotified(Context context, Resource resource) {
		// TODO Auto-generated method stub
		
	}
	
	
}
