package org.gcube.vremanagement.contextmanager.operators.resources;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidContextException;
import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidParameterException;
import org.gcube.vremanagement.contextmanager.model.exceptions.OperationException;
import org.gcube.vremanagement.contextmanager.model.operators.context.CustomContextOperator;
import org.gcube.vremanagement.contextmanager.model.operators.parameters.OperatorParameters;
import org.gcube.vremanagement.contextmanager.model.operators.parameters.RemoveResourceParameters;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.gcube.vremanagement.contextmanager.model.types.Context.Type;
import org.gcube.vremanagement.contextmanager.model.types.ScopedResource;
import org.slf4j.Logger;

public class RemoveResourceOperator extends CustomContextOperator<ScopedResource, RemoveResourceParameters> {

	@Inject
	private Logger log;

	@Inject
	private ResourceManager resourceManager;


	@Override
	public Set<Type> getAllowedContextType() {
		return new HashSet<>(Arrays.asList(Type.values()));
	}

	@Override
	public String getOperationId() {
		return "remove-resource";
	}

	@Override
	public String getDescription() {
		return "Removes resource from context";
	}

	@Override
	public ScopedResource execute(Context context, RemoveResourceParameters params) throws OperationException {
		log.debug("executing operation {}",this.getOperationId());
		try {
			ScopedResource res = resourceManager.removeResourceFromContext(context, params.getResourceId());
			return res;
		}catch (InvalidContextException e) {
			//it should never happen
			throw new InvalidParameterException("unknown exception");
		} 
	}

	@Override
	public ScopedResource undo(Context context, OperatorParameters params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected RemoveResourceParameters checkAndTrasformParameters(Context context, OperatorParameters params)
			throws InvalidParameterException {
		RemoveResourceParameters removeParams = RemoveResourceParameters.class.cast(params);
		if (removeParams.getResourceId()==null)
			throw new InvalidParameterException("resourceId is null");
		return removeParams;
	}


}
