package org.gcube.vremanagement.contextmanager.operators.resources;

import java.util.Arrays;
import java.util.List;

import javax.inject.Singleton;

import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.resources.gcore.Resource.Type;
import org.gcube.vremanagement.contextmanager.model.types.Context;

@Singleton
public class DynamicResourceHandler implements ResourceHandler {

	
	@Override
	public List<Type> getManagedResources() {
		return Arrays.asList(Type.GCOREENDPOINT, Type.NODE);
	}

	@Override
	public boolean addResource(Context context, Resource resource) {
		return false;
	}

	@Override
	public boolean removeResource(Context context, Resource resource) {
		return false;		
	}

	@Override
	public void createResourceNotified(Context context, Resource resource) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeResourceNotified(Context context, Resource resource) {
		// TODO Auto-generated method stub
		
	}
	
}
