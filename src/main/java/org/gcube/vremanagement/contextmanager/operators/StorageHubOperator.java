package org.gcube.vremanagement.contextmanager.operators;

import java.util.Collections;
import java.util.Set;

import org.gcube.common.authorization.library.provider.AuthorizationProvider;
import org.gcube.common.storagehub.client.dsl.StorageHubClient;
import org.gcube.common.storagehub.model.acls.AccessType;
import org.gcube.common.storagehub.model.exceptions.StorageHubException;
import org.gcube.vremanagement.contextmanager.model.operators.context.MandatoryContextOperator;
import org.gcube.vremanagement.contextmanager.model.report.OperationResult;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.gcube.vremanagement.contextmanager.model.types.Context.Type;

public class StorageHubOperator implements MandatoryContextOperator{
	
	StorageHubClient client = new StorageHubClient();
	
	@Override
	public OperationResult onCreate(Context context) {		
		
		try {
			client.getVreFolderManager().createVRE(AccessType.WRITE_OWNER, AuthorizationProvider.instance.get().getClient().getId());
		} catch (StorageHubException e) {
			return OperationResult.failure(e.getErrorMessage());
		}	
		/*try {
			for (String user: users)
			client.getVreFolderManager().addUser(userId);			
		}catch ( e) {
			// TODO: handle exception
		}*/
		return OperationResult.success();
	}

	@Override
	public OperationResult onDispose(Context context) {
		try {
			client.getVreFolderManager().removeVRE();
		} catch (StorageHubException e) {
			
		}	
		return null;
		
	}

	
	public String getDescription() {
		return "creates/removes all the needed stuff in Storagehub";
	}
	
	@Override
	public String getOperationId() {
		return "storagehub-operations";
	}

	@Override
	public Set<Type> getAllowedContextType() {
		return Collections.singleton(Context.Type.VRE);
	}
}
