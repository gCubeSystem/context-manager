package org.gcube.vremanagement.contextmanager;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import org.gcube.vremanagement.contextmanager.model.types.ScopedResource;

@XmlRootElement(name = "Body")
@XmlAccessorType (XmlAccessType.FIELD)
public class ScopeDescriptor {

	@XmlElement(name = "Scope")
	String context;
	
	
	@XmlElement(name = "Service")
	String service;
	
	@XmlElement(name = "Manager")
	String manager;
	
	@XmlElement(name = "Designer")
	String designer;
	
	@XmlElement(name = "StartTime")
	String startTime;
	
	@XmlElement(name = "SecurityEnabled")
	boolean secure = false;
		


	@XmlElementWrapper(name="ScopedRescources")
    @XmlElement(name="ScopedRescource")
	List<ScopedResource> scopedResources =  null;


	@Override
	public String toString() {
		return "ScopeDescriptor [context=" + context + ", manager=" + manager + ", designer=" + designer
				+ ", startTime=" + startTime + ", secure=" + secure + ", scopedResources=" + scopedResources + "]";
	}
	
	
}
