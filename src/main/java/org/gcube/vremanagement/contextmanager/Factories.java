package org.gcube.vremanagement.contextmanager;

import javax.enterprise.inject.Produces;
import javax.enterprise.inject.spi.InjectionPoint;

import org.gcube.smartgears.ApplicationManagerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

class Factories {

	@Produces public Logger createLogger(InjectionPoint injectionPoint) {
		return LoggerFactory.getLogger(injectionPoint.getMember().getDeclaringClass());
	}

	@Produces public ContextServiceAppManager createAppManager() {
		return (ContextServiceAppManager)ApplicationManagerProvider.get();
	}
}
