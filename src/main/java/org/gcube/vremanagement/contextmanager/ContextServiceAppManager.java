package org.gcube.vremanagement.contextmanager;


import java.util.HashSet;
import java.util.Set;

import javax.inject.Inject;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.smartgears.ApplicationManager;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.slf4j.Logger;

public class ContextServiceAppManager implements ApplicationManager {

    @Inject Logger logger;
	
	@Inject ScopeInitializer scopeInitializer;
	
	private static Set<String> managedScope =new HashSet<>();
	
	@Override
	public void onInit() {
		String currentContext = ScopeProvider.instance.get();
		scopeInitializer.initScope(currentContext);
		managedScope.add(currentContext);
	}

	@Override
	public void onShutdown() {
	}

	public boolean isManaged(Context context){
		if (context.getType()==Context.Type.VRE)
			return managedScope.contains(context.getParent().getId());
		else 
			return managedScope.contains(context.getId());
	}

	
}
