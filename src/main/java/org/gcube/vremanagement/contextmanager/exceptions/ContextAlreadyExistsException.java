package org.gcube.vremanagement.contextmanager.exceptions;

public class ContextAlreadyExistsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5847389374215009478L;

	public ContextAlreadyExistsException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ContextAlreadyExistsException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	public ContextAlreadyExistsException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ContextAlreadyExistsException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public ContextAlreadyExistsException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}
	
}
