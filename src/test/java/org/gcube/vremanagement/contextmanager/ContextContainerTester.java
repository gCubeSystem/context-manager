package org.gcube.vremanagement.contextmanager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.enterprise.inject.Alternative;
import javax.enterprise.inject.Default;
import javax.inject.Singleton;

import org.gcube.vremanagement.contextmanager.exceptions.ContextAlreadyExistsException;
import org.gcube.vremanagement.contextmanager.handlers.ContextContainer;
import org.gcube.vremanagement.contextmanager.handlers.impl.ContextTree;
import org.gcube.vremanagement.contextmanager.handlers.impl.TreeItem;
import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidContextException;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.gcube.vremanagement.contextmanager.model.types.Context.Type;


public class ContextContainerTester {

	/*
	private Map<String, List<String>> resourceMap = new HashMap<>();

	private static ContextTree contextTree = new ContextTree() {

		@Override
		public void init() {
			try {
				TreeItem root = this.createItem(null, new Context(null, "gcube", "gcube", Type.INFRASTRUCTURE));
				TreeItem devsec = this.createItem(root.getContext().getId(), new Context(root.getContext(), "devsec", "devsec", Type.VO));
				TreeItem devNext = this.createItem(root.getContext().getId(), new Context(root.getContext(), "devNext", "devNext", Type.VO));
				this.createItem(devsec.getContext().getId(), new Context(devsec.getContext(),"devVRE", "devVRE", Type.VRE));
				this.createItem(devNext.getContext().getId(), new Context(devNext.getContext(),"nextNext", "nextNext", Type.VRE));
			}catch (InvalidContextException e) {}
		}
	};

	@Override
	public List<String> getAvailableContexts() {
		return contextTree.getContexts();
	}

	@Override
	public void addContext(Context toAdd) throws InvalidContextException, ContextAlreadyExistsException{
		contextTree.createItem(toAdd==null?null:toAdd.getId(), toAdd);
	}

	@Override
	public void removeContext(String contextId) throws InvalidContextException {
		TreeItem item = contextTree.removeItem(contextId);
		if (item==null) throw new InvalidContextException("context with id "+contextId+" not found");
		resourceMap.remove(item.getContext().getId());
	}

	@Override
	public Context getContextById(String id) throws InvalidContextException {
		Context context = contextTree.getContext(id);
		if (context==null) throw new InvalidContextException("context with id "+id+" not found");
		return context;
	}

	@Override
	public synchronized void addResource(String contextId, String resourceId) throws InvalidContextException {
		Context context = contextTree.getContext(contextId);
		if (context==null) throw new InvalidContextException("context with id "+contextId+" not found");

		if (!resourceMap.containsKey(contextId))
			resourceMap.put(contextId, new ArrayList<>()); 
		resourceMap.get(contextId).add(resourceId);
		
	}

	@Override
	public synchronized void removeResource(String contextId, String resourceId) throws InvalidContextException {
		Context context = contextTree.getContext(contextId);
		if (context==null) throw new InvalidContextException("context with id "+contextId+" not found");

		if (resourceMap.containsKey(contextId))
			resourceMap.get(contextId).remove(resourceId);
	}

	@Override
	public List<String> getResources(String contextId) throws InvalidContextException {
		Context context = contextTree.getContext(contextId);
		if (context==null) throw new InvalidContextException("context with id "+contextId+" not found");
		if (resourceMap.containsKey(contextId))
			return resourceMap.get(contextId);
		else return Collections.emptyList();
	}
 */
}
