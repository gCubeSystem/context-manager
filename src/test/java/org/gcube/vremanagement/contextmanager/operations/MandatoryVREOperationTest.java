package org.gcube.vremanagement.contextmanager.operations;

import java.util.Collections;
import java.util.Set;

import javax.inject.Inject;

import org.gcube.vremanagement.contextmanager.model.operators.context.MandatoryContextOperator;
import org.gcube.vremanagement.contextmanager.model.report.OperationResult;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.gcube.vremanagement.contextmanager.model.types.Context.Type;
import org.slf4j.Logger;

public class MandatoryVREOperationTest implements MandatoryContextOperator{

	
	@Inject Logger log;
	
	@Override
	public String getOperationId() {
		return "test-op";
	}

	
	@Override
	public String getDescription() {
		return "testOp";
	}

	@Override
	public OperationResult onCreate(Context context) {
		log.debug("executing testOP");
		return OperationResult.success();
	}

	@Override
	public OperationResult onDispose(Context context) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Type> getAllowedContextType() {
		return Collections.singleton(Type.VRE);
	}

}
