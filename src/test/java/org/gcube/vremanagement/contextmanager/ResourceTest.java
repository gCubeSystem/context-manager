package org.gcube.vremanagement.contextmanager;

import static org.gcube.resources.discovery.icclient.ICFactory.clientFor;
import static org.gcube.resources.discovery.icclient.ICFactory.queryFor;

import java.util.List;

import org.gcube.common.resources.gcore.GenericResource;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.resources.discovery.client.api.DiscoveryClient;
import org.gcube.resources.discovery.client.queries.api.SimpleQuery;
import org.junit.Before;
import org.junit.Test;

public class ResourceTest {
	
	@Before
	public void init() {
		ScopeProvider.instance.set("/gcube/devsec");		
	}
	
	@Test
	public void bindResource() throws Exception{

		SimpleQuery query = queryFor(GenericResource.class);
		query.addCondition("$resource/Profile/SecondaryType/text() eq 'VO'");
		
		DiscoveryClient<GenericResource> client = clientFor(GenericResource.class);
		List<GenericResource> resources = client.submit(query);
		
		System.out.println(resources.get(0).profile().bodyAsString());
		
		System.out.println(ResourceBinder.get().bind("<Body>"+resources.get(0).profile().bodyAsString()+"</Body>"));
		
	}
	
	
	@Test
	public void regExpr() {
		String regexpr = ".*(?=/resources)";
		String val = "/gcube/devsec/devVRE/resources";
		System.out.println(val.replaceFirst(regexpr, "ciao"));
	
	}
}
