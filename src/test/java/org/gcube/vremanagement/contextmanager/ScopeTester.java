package org.gcube.vremanagement.contextmanager;

import static org.junit.Assert.assertTrue;

import java.util.Collections;

import javax.inject.Inject;

import org.gcube.common.resources.gcore.Resource;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.vremanagement.contextmanager.collector.LegacyISConnector;
import org.gcube.vremanagement.contextmanager.handlers.ContextContainer;
import org.gcube.vremanagement.contextmanager.handlers.impl.ContextContainerImpl;
import org.gcube.vremanagement.contextmanager.model.exceptions.InvalidContextException;
import org.gcube.vremanagement.contextmanager.model.exceptions.OperationException;
import org.gcube.vremanagement.contextmanager.model.operators.parameters.AddResourceParameters;
import org.gcube.vremanagement.contextmanager.model.operators.parameters.RemoveResourceParameters;
import org.gcube.vremanagement.contextmanager.model.types.Context;
import org.gcube.vremanagement.contextmanager.operators.resources.AddResourceOperator;
import org.gcube.vremanagement.contextmanager.operators.resources.DynamicResourceHandler;
import org.gcube.vremanagement.contextmanager.operators.resources.RemoveResourceOperator;
import org.gcube.vremanagement.contextmanager.operators.resources.StaticResourceHandler;
import org.jglue.cdiunit.AdditionalClasses;
import org.jglue.cdiunit.CdiRunner;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;

@RunWith(CdiRunner.class)
@AdditionalClasses({ContextContainerImpl.class, Factories.class, 
	 DynamicResourceHandler.class, StaticResourceHandler.class })
public class ScopeTester {

	@Inject ScopeInitializer scopeInitializer;
	
	@Inject ContextContainer container;
	
	@Inject ContextManager contextManager;
	
	@Inject LegacyISConnector is;
	
	@Inject Logger log;
	
	@Inject AddResourceOperator addResourceOp;
	
	@Inject RemoveResourceOperator removeResourceOp;
	
	private static String resourceId = "eabfe9bb-001d-48bc-92dc-586eefdc2006";
	
	private static String context = "/gcube/devNext";
	
	private static String vreContext = "/gcube/devNext/NextNext";
	
	@Before
	public void init() {
		ScopeProvider.instance.set(context);
		scopeInitializer.initScope(context);
		
	}
	
	@Test
	public void initTest() {
		
		container.getAvailableContexts().forEach(c -> {
			try {
				container.getResources(c).forEach(r-> log.debug(" {} res {} ",c,r));
			} catch (InvalidContextException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		
	}
	
	@Test
	public void addAndRemoveResourceScopeTest() throws InvalidContextException {
		Context current = container.getContextById(ScopeProvider.instance.get());
		try {
			addResourceOp.execute(current, new AddResourceParameters(vreContext, resourceId, Resource.Type.GENERIC));
		} catch (OperationException e) {
			log.error("operation error",e);
		}
		assertTrue(container.getResources(vreContext).contains(resourceId));
		Resource res = is.find(resourceId);
		assertTrue(res.scopes().asCollection().contains(vreContext));
		log.debug("scopes are {}",res.scopes().asCollection());
		try {
			removeResourceOp.execute(current, new RemoveResourceParameters(vreContext, resourceId));
		} catch (OperationException e) {
			log.error("operation error",e);
		}
		assertTrue(!container.getResources(vreContext).contains(resourceId));
		assertTrue(!is.find(resourceId).scopes().contains(vreContext));
	}
	
	@Test
	public void createContextTest() throws InvalidContextException {
		contextManager.createContext(context, "newVRE", Collections.emptyList(), "user");
		assertTrue(container.getAvailableContexts().contains("newVRE"));
	}
	
}
