package org.gcube.vremanagement.contextmanager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.gcube.vremanagement.contextmanager.model.types.StringList;
import org.gcube.vremanagement.contextmanager.services.ContextService;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.client.ClientConfig;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.media.multipart.FormDataMultiPart;
import org.glassfish.jersey.media.multipart.MultiPartFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.jboss.weld.environment.se.Weld;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ContextServiceIntegrationTest extends JerseyTest {

	Logger log = LoggerFactory.getLogger(ContextServiceIntegrationTest.class); 
	
	@Override
	protected Application configure() {
		/*Class<ExternalRequestScope>[] extScopes = ServiceFinder.find(ExternalRequestScope.class, true).toClassArray();
    	 for (Class<ExternalRequestScope> ers : extScopes) {
    		 System.out.println("ESR "+ers.getName());
    	 }*/

		final Weld weld = new Weld();
		weld.initialize();
		Runtime.getRuntime().addShutdownHook(new Thread(() -> weld.shutdown()));

		return new ResourceConfig(ContextService.class)
				.register(MultiPartFeature.class)
				.register(LoggingFeature.class).register(new AbstractBinder() {
				      @Override
				      protected void configure() {
				        bind(log).to(Logger.class);
				      }
				    });    
	}



	@Test
	public void getContexts_whenCorrectRequest_thenResponseIsOk() {

		Response response = target("/contexts").request().get();

		assertEquals("Http Response should be 200: ", Status.OK.getStatusCode(), response.getStatus());
		assertEquals("Http Content-Type should be: ", MediaType.APPLICATION_JSON, response.getHeaderString(HttpHeaders.CONTENT_TYPE));

		assertTrue(response.readEntity(StringList.class).getValues().size()>0);
	}

	@Test
	public void createVREContexts_whenCorrectRequest_thenResponseIsOk() {

		List<String> resourcesList = Arrays.asList("id1", "id2");
		try (final FormDataMultiPart multipart = new FormDataMultiPart()
				.field("resources", new StringList(resourcesList),  MediaType.APPLICATION_JSON_TYPE)
				.field("vreName", "testVRE")){
			Response response = target("/contexts/devsec").request().put(Entity.entity(multipart, MediaType.MULTIPART_FORM_DATA));
			assertEquals("Http Response should be 204: ", Status.NO_CONTENT.getStatusCode(), response.getStatus());
				
		}catch (Exception e) {
			e.printStackTrace();
		}

		Response response1 = target("/contexts").request().get();

		assertTrue(response1.readEntity(StringList.class).getValues().contains("/gcube/devsec/testVRE"));
		
		Response response2 = target("/contexts/testVRE/resources").request().get();
		
		assertTrue(response2.readEntity(StringList.class).getValues().containsAll(resourcesList));
	}
	
	@Test
	public void diposeVREContexts_whenCorrectRequest_thenResponseIsOk() {

		Response response = target("/contexts/devVRE").request().delete();
		
		assertEquals("Http Response should be 204: ", Status.NO_CONTENT.getStatusCode(), response.getStatus());
		
		Response response1 = target("/contexts").request().get();

		assertTrue(!response1.readEntity(StringList.class).getValues().contains("/gcube/devsec/devVRE"));
		
		Response response2 = target("/contexts/devVRE/resources").request().get();
		
		assertNotEquals("Http Response should not be 200: ", Status.OK.getStatusCode(), response2.getStatus());
	}
	
}
